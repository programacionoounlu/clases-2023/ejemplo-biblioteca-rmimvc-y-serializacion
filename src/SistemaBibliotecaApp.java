import ar.edu.unlu.poo.biblioteca.controladores.Controlador;
import ar.edu.unlu.poo.biblioteca.modelos.Libro;
import ar.edu.unlu.poo.biblioteca.modelos.ModeloLibros;
import ar.edu.unlu.poo.biblioteca.vistas.IVista;
import ar.edu.unlu.poo.biblioteca.vistas.VistaConsola;
import ar.edu.unlu.poo.biblioteca.vistas.VistaGrafica;
import ar.edu.unlu.poo.biblioteca.vistas.consolagrafica.ConsolaGrafica;

import java.io.*;
import java.util.ArrayList;
import java.util.Scanner;

public class SistemaBibliotecaApp {
    public static void main(String[] args) {
        // ejemplo de Serialización

        ArrayList<Libro> libros = new ArrayList<>();

        libros.add(new Libro("Cien años de soledad", "Gabriel García Márquez", 15));
        libros.add(new Libro("El Gran Gatsby", "F. Scott Fitzgerald", 10));
        libros.add(new Libro("1984", "George Orwell", 8));

        System.out.println(libros);

        try {
            FileOutputStream fos = new FileOutputStream("serializado.bin");
            var oos = new ObjectOutputStream(fos);
            oos.writeObject(libros);
            fos.close();
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        try {
            FileInputStream fos = new FileInputStream("serializado.bin");
            var oos = new ObjectInputStream(fos);
            var obj = (ArrayList<Libro>) oos.readObject();
            System.out.println(libros);
            fos.close();
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            throw new RuntimeException(e);
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }

//        modelo.agregarLibro(libro1);
//        modelo.agregarLibro(libro2);
//        modelo.agregarLibro(libro3);
//        modelo.agregarLibro(libro4);
//        modelo.agregarLibro(libro5);
//        modelo.agregarLibro(libro6);
//        modelo.agregarLibro(libro7);
//        modelo.agregarLibro(libro8);
//        modelo.agregarLibro(libro9);
//        modelo.agregarLibro(libro10);
//
//        //IVista vista = new VistaConsola();
//        //IVista vista = new VistaGrafica();
//        IVista vista = new ConsolaGrafica();
//        Controlador controlador = new Controlador(vista);
//        controlador.setModelo(modelo);
//        vista.mostrarMenuPrincipal();
//        // temporal
//        vista.mostrarResultadosBusqueda(modelo.obtenerTodosLosLibros());
//
//        IVista vistaG = new VistaGrafica();
//        Controlador controladorG = new Controlador(vistaG);
//        controladorG.setModelo(modelo);
//        vistaG.mostrarMenuPrincipal();
//        // temporal
//        vistaG.mostrarResultadosBusqueda(modelo.obtenerTodosLosLibros());
    }
}
