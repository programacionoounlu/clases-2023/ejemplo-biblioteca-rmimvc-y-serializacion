package ar.edu.unlu.poo.biblioteca;

import ar.edu.unlu.poo.biblioteca.modelos.IModeloLibros;
import ar.edu.unlu.poo.biblioteca.modelos.Libro;
import ar.edu.unlu.poo.biblioteca.modelos.ModeloLibros;
import ar.edu.unlu.rmimvc.RMIMVCException;
import ar.edu.unlu.rmimvc.Util;
import ar.edu.unlu.rmimvc.servidor.Servidor;

import javax.swing.*;
import java.rmi.RemoteException;
import java.util.ArrayList;

public class AppServidor {
    public static void main(String[] args) {
        ArrayList<String> ips = Util.getIpDisponibles();
        String ip = (String) JOptionPane.showInputDialog(
                null,
                "Seleccione la IP en la que escuchará peticiones el servidor", "IP del servidor",
                JOptionPane.QUESTION_MESSAGE,
                null,
                ips.toArray(),
                null
        );
        String port = (String) JOptionPane.showInputDialog(
                null,
                "Seleccione el puerto en el que escuchará peticiones el servidor", "Puerto del servidor",
                JOptionPane.QUESTION_MESSAGE,
                null,
                null,
                8888
        );

        IModeloLibros modelo = new ModeloLibros();

        Libro libro1 = new Libro("El Gran Gatsby", "F. Scott Fitzgerald", 10);
        Libro libro2 = new Libro("Cien años de soledad", "Gabriel García Márquez", 15);
        Libro libro3 = new Libro("1984", "George Orwell", 8);
        Libro libro4 = new Libro("To Kill a Mockingbird", "Harper Lee", 12);
        Libro libro5 = new Libro("Matar un ruiseñor", "Harper Lee", 9);
        Libro libro6 = new Libro("Dune", "Frank Herbert", 8);
        Libro libro7 = new Libro("Fundación", "Isaac Asimov", 10);
        Libro libro8 = new Libro("Neuromante", "William Gibson", 6);
        Libro libro9 = new Libro("Segunda Fundación", "Isaac Asimov", 7);
        Libro libro10 = new Libro("¿Sueñan los androides con ovejas eléctricas?", "Philip K. Dick", 9);

        try {
            modelo.agregarLibro(libro1);
            modelo.agregarLibro(libro2);
            modelo.agregarLibro(libro3);
            modelo.agregarLibro(libro4);
            modelo.agregarLibro(libro5);
            modelo.agregarLibro(libro6);
            modelo.agregarLibro(libro7);
            modelo.agregarLibro(libro8);
            modelo.agregarLibro(libro9);
            modelo.agregarLibro(libro10);
        } catch (Exception e) {
            e.printStackTrace();
        }

        Servidor servidor = new Servidor(ip, Integer.parseInt(port));
        try {
            servidor.iniciar(modelo);
        } catch (RemoteException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (RMIMVCException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
}
