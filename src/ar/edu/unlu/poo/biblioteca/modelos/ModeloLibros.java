package ar.edu.unlu.poo.biblioteca.modelos;

import ar.edu.unlu.poo.biblioteca.modelos.exceptions.LibroNoEncontrado;
import ar.edu.unlu.poo.biblioteca.modelos.exceptions.LibroNoDisponible;
import ar.edu.unlu.rmimvc.observer.IObservadorRemoto;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;

public class ModeloLibros implements IModeloLibros {
    private List<Libro> listaDeLibros;

    public ModeloLibros() {
        listaDeLibros = new ArrayList<>();
    }

    @Override
    public List<Libro> obtenerTodosLosLibros() throws RemoteException {
        return listaDeLibros;
    }

    @Override
    public Libro obtenerLibroPorId(int id) throws RemoteException {
        for (Libro libro : listaDeLibros) {
            if (libro.getId() == id) {
                return libro;
            }
        }
        return null; // Devolver null si no se encuentra el libro
    }

    @Override
    public Libro agregarLibro(Libro libro) throws RemoteException {
        listaDeLibros.add(libro);
        return libro;
    }

    @Override
    public List<Libro> buscarLibrosPorTitulo(String parteDelTitulo) throws RemoteException {
        List<Libro> librosEncontrados = new ArrayList<>();

        for (Libro libro : listaDeLibros) {
            if (libro.getTitulo().toLowerCase().contains(parteDelTitulo.toLowerCase())) {
                librosEncontrados.add(libro);
            }
        }

        return librosEncontrados;
    }

    @Override
    public void prestarLibro(int id) throws LibroNoEncontrado, LibroNoDisponible, RemoteException {
        Libro libro = obtenerLibroPorId(id);
        if (libro == null)
            throw new LibroNoEncontrado();
        if (!libro.estaDisponible())
            throw new LibroNoDisponible();
        libro.prestar();
    }

    @Override
    public void devolverLibro(int id) throws LibroNoEncontrado, LibroNoDisponible, RemoteException {
        Libro libro = obtenerLibroPorId(id);
        if (libro == null)
            throw new LibroNoEncontrado();
        if (!libro.hayPrestados())
            throw new LibroNoDisponible();
        libro.devolver();
    }

    @Override
    public void agregarObservador(IObservadorRemoto iObservadorRemoto) throws RemoteException {

    }

    @Override
    public void removerObservador(IObservadorRemoto iObservadorRemoto) throws RemoteException {

    }

    @Override
    public void notificarObservadores(Object o) throws RemoteException {

    }

    @Override
    public void notificarObservadores() throws RemoteException {

    }

    // Puedes agregar más métodos según las operaciones necesarias, como buscar, prestar, devolver, etc.
}
