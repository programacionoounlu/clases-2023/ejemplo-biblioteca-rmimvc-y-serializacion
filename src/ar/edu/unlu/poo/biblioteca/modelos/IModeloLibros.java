package ar.edu.unlu.poo.biblioteca.modelos;

import ar.edu.unlu.poo.biblioteca.modelos.exceptions.LibroNoDisponible;
import ar.edu.unlu.poo.biblioteca.modelos.exceptions.LibroNoEncontrado;
import ar.edu.unlu.rmimvc.observer.IObservableRemoto;

import java.rmi.RemoteException;
import java.util.List;

public interface IModeloLibros extends IObservableRemoto {
    List<Libro> obtenerTodosLosLibros() throws RemoteException;

    Libro obtenerLibroPorId(int id) throws RemoteException;

    Libro agregarLibro(Libro libro) throws RemoteException;

    List<Libro> buscarLibrosPorTitulo(String parteDelTitulo) throws RemoteException;

    void prestarLibro(int id) throws LibroNoEncontrado, LibroNoDisponible, RemoteException;

    void devolverLibro(int id) throws LibroNoEncontrado, LibroNoDisponible, RemoteException;
}
