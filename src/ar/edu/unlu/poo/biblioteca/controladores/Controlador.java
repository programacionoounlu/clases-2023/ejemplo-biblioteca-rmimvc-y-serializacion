package ar.edu.unlu.poo.biblioteca.controladores;

import ar.edu.unlu.poo.biblioteca.modelos.IModeloLibros;
import ar.edu.unlu.poo.biblioteca.modelos.Libro;
import ar.edu.unlu.poo.biblioteca.modelos.ModeloLibros;
import ar.edu.unlu.poo.biblioteca.modelos.exceptions.LibroNoEncontrado;
import ar.edu.unlu.poo.biblioteca.modelos.exceptions.LibroNoDisponible;
import ar.edu.unlu.poo.biblioteca.vistas.IVista;
import ar.edu.unlu.rmimvc.cliente.IControladorRemoto;
import ar.edu.unlu.rmimvc.observer.IObservableRemoto;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;

public class Controlador implements IControladorRemoto {
    private final IVista vista;
    private IModeloLibros modelo;

    public Controlador(IVista vista) {
        this.vista = vista;
        vista.setControlador(this);
    }
    public void agregarLibro(String titulo, String autor, int ejemplares) {
        try {
            modelo.agregarLibro(new Libro(titulo, autor, ejemplares));
        } catch (RemoteException e) {
            throw new RuntimeException(e);
        }
    }

    public void prestarLibro(int id) throws LibroNoDisponible, LibroNoEncontrado {
        try {
            modelo.prestarLibro(id);
        } catch (RemoteException e) {
            throw new RuntimeException(e);
        }
    }

    public void devolverLibro(int id) throws LibroNoEncontrado, LibroNoDisponible {
        try {
            modelo.devolverLibro(id);
        } catch (RemoteException e) {
            throw new RuntimeException(e);
        }
    }

    public List<Libro> obtenerTodosLosLibros() {
        try {
            return modelo.obtenerTodosLosLibros();
        } catch (RemoteException e) {
            throw new RuntimeException(e);
        }
    }

    public Libro obtenerLibroPorId(int id) {
        try {
            return modelo.obtenerLibroPorId(id);
        } catch (RemoteException e) {
            throw new RuntimeException(e);
        }
    }

    public List<Libro> buscarLibrosPorTitulo(String parteDelTitulo) {
        try {
            return modelo.buscarLibrosPorTitulo(parteDelTitulo);
        } catch (RemoteException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public <T extends IObservableRemoto> void setModeloRemoto(T modeloRemoto) throws RemoteException {
        this.modelo = (IModeloLibros) modeloRemoto; // es necesario castear el modelo remoto
    }

    @Override
    public void actualizar(IObservableRemoto iObservableRemoto, Object o) throws RemoteException {

    }
}
