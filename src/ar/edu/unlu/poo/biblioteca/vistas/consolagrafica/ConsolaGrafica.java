package ar.edu.unlu.poo.biblioteca.vistas.consolagrafica;

import ar.edu.unlu.poo.biblioteca.controladores.Controlador;
import ar.edu.unlu.poo.biblioteca.modelos.Libro;
import ar.edu.unlu.poo.biblioteca.vistas.IVista;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

public class ConsolaGrafica implements IVista {
    private final JFrame frame;
    private JPanel contentPane;
    private JTextArea txtSalidaConsola;
    private JTextField txtEntrada;
    private JButton btnEnter;
    private Controlador controlador;

    private Flujo flujoActual;

    private void createUIComponents() {
        // TODO: place custom component creation code here
    }

    public ConsolaGrafica() {
        frame = new JFrame("<class name>");
        frame.setContentPane(contentPane);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();

        btnEnter.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                txtSalidaConsola.append(txtEntrada.getText() + "\n");
                procesarEntrada(txtEntrada.getText());
                txtEntrada.setText("");
            }
        });
    }

    private void procesarEntrada(String input) {
        input = input.trim();
        if (input.isEmpty())
            return;
        flujoActual = flujoActual.procesarEntrada(input);
        flujoActual.mostarSiguienteTexto();
    }
    
    public void println(String texto) {
        txtSalidaConsola.append(texto + "\n");
    }
    


    public void mostrar() {
        frame.setVisible(true);
    }

    @Override
    public void setControlador(Controlador controlador) {
        this.controlador = controlador;
    }

    @Override
    public void mostrarAltaLibro() {

    }

    @Override
    public void mostrarDevolucionLibro() {

    }

    @Override
    public void mostrarBusquedaLibros() {

    }

    @Override
    public void mostrarPrestamoLibros() {

    }

    @Override
    public void mostrarResultadoPrestamoExitoso() {

    }

    @Override
    public void mostrarResultadoPrestamoFallido() {

    }

    @Override
    public void mostrarResultadoDevolucionExitosa() {

    }

    @Override
    public void mostrarResultadoDevolucionFallida() {

    }

    @Override
    public void mostrarResultadosBusqueda(List<Libro> librosEncontrados) {

    }

    @Override
    public void mostrarMenuPrincipal() {
        mostrar();
        flujoActual = new FlujoMenuPrincipal(this, controlador);
        flujoActual.mostarSiguienteTexto();
    }
}
