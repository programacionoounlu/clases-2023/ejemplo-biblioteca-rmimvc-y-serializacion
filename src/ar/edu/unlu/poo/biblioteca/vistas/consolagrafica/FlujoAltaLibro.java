package ar.edu.unlu.poo.biblioteca.vistas.consolagrafica;

import ar.edu.unlu.poo.biblioteca.controladores.Controlador;
import ar.edu.unlu.poo.biblioteca.vistas.IVista;

public class FlujoAltaLibro extends Flujo {
    public FlujoAltaLibro(ConsolaGrafica vista, Controlador controlador) {
        super(vista, controlador);
    }

    enum EstadosPosibles {
        INGRESANDO_TITULO,
        INGRESANDO_AUTOR,
        INGRESANDO_EJEMPLARES
    }

    private EstadosPosibles estadoActual = EstadosPosibles.INGRESANDO_TITULO;

    private String titulo;
    private String autor;

    @Override
    public Flujo procesarEntrada(String input) {
        switch (estadoActual) {
            case INGRESANDO_TITULO -> procesarIngresoTitulo(input);
            case INGRESANDO_AUTOR -> procesarIngresoAutor(input);
            case INGRESANDO_EJEMPLARES -> {
                return procesarIngresoEjemplares(input);
            }
        }
        return this;
    }

    private Flujo procesarIngresoEjemplares(String input) {
        Integer ejemplares;
        try {
            ejemplares = Integer.parseInt(input);
            if (ejemplares <= 0) {
                vista.println("Ingrese un valor > 0");
            }
            controlador.agregarLibro(titulo, autor, ejemplares);
            vista.println("Se creó el libro.");
            return new FlujoMenuPrincipal(vista, controlador);
        } catch (NumberFormatException e) {
            vista.println("Ingrese un número válido.");
        }
        return this;
    }

    private void procesarIngresoAutor(String input) {
        autor = input;
        estadoActual = EstadosPosibles.INGRESANDO_EJEMPLARES;
    }

    private void procesarIngresoTitulo(String input) {
        titulo = input;
        estadoActual = EstadosPosibles.INGRESANDO_AUTOR;
    }

    @Override
    public void mostarSiguienteTexto() {
        switch (estadoActual) {
            case INGRESANDO_TITULO -> {
                vista.println("Ingrese los datos del nuevo libro:");
                vista.println("Titulo: ");
            }
            case INGRESANDO_AUTOR -> vista.println("Autor: ");
            case INGRESANDO_EJEMPLARES -> vista.println("Cantidad de ejemplares: ");
        }
    }
}
