package ar.edu.unlu.poo.biblioteca.vistas.consolagrafica;

import ar.edu.unlu.poo.biblioteca.controladores.Controlador;
import ar.edu.unlu.poo.biblioteca.modelos.Libro;

public class FlujoMenuPrincipal extends Flujo {
    public FlujoMenuPrincipal(ConsolaGrafica vista, Controlador controlador) {
        super(vista, controlador);
    }

    @Override
    public Flujo procesarEntrada(String string) {
        switch (string) {
            case "1" -> mostrarTodosLosLibros();
            case "2" -> {
                return new FlujoAltaLibro(vista, controlador);
            }
            case "3" -> vista.println("Opcion 3");
            case "4" -> vista.println("Opcion 4");
            case "5" -> vista.println("Opcion 5");
            default -> vista.println("Opción inválida");
        }
        return this;
    }

    private void mostrarTodosLosLibros() {
        for (Libro libro: controlador.obtenerTodosLosLibros()) {
            vista.println(libro.toString());
        }
        vista.println("------");
    }

    @Override
    public void mostarSiguienteTexto() {
        vista.println("Menú Principal:");
        vista.println("1. Mostrar todos los Libros");
        vista.println("2. Alta de Libro");
        vista.println("3. Préstamo de Libro");
        vista.println("4. Devolución de Libro");
        vista.println("5. Salir");
        vista.println("Seleccione una opción: ");
    }

}
