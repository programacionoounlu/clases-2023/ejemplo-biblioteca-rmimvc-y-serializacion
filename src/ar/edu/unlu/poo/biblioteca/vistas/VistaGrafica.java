package ar.edu.unlu.poo.biblioteca.vistas;

import ar.edu.unlu.poo.biblioteca.controladores.Controlador;
import ar.edu.unlu.poo.biblioteca.modelos.Libro;
import ar.edu.unlu.poo.biblioteca.modelos.exceptions.LibroNoDisponible;
import ar.edu.unlu.poo.biblioteca.modelos.exceptions.LibroNoEncontrado;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

public class VistaGrafica implements IVista {
    private final JFrame frame;
    private JTabbedPane tabbedPane;
    private JPanel contentPane;
    private JList lstLibros;
    private JTextField txtTitulo;
    private JTextField txtAutor;
    private JSpinner spEjemplares;
    private JButton confirmarButton;
    private JTextField txtIdLibro;
    private JButton prestarButton;
    private JComboBox<Libro> cbLibroDevolucion;
    private JButton devolverButton;
    private Controlador controlador;

    private DefaultListModel<String> listModel;

    public VistaGrafica() {
        frame = new JFrame();
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.setContentPane(contentPane);
        frame.pack();

        listModel = new DefaultListModel<>();
        lstLibros.setModel(listModel);
        confirmarButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                procesarAltaDeLibro();
            }
        });

        prestarButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                procesarPrestarLibro();
            }
        });
        devolverButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Libro libro = (Libro) cbLibroDevolucion.getSelectedItem();
                try {
                    controlador.devolverLibro(libro.getId());
                } catch (LibroNoEncontrado ex) {
                    throw new RuntimeException(ex);
                } catch (LibroNoDisponible ex) {
                    throw new RuntimeException(ex);
                }
                // esta línea se va cuando esté implementado Observer
                mostrarResultadosBusqueda(controlador.obtenerTodosLosLibros());
            }
        });
    }

    private void procesarPrestarLibro() {
        int idLibro = Integer.parseInt(txtIdLibro.getText());
        try {
            controlador.prestarLibro(idLibro);
            txtIdLibro.setText("");
            // esta línea se va cuando esté implementado Observer
            mostrarResultadosBusqueda(controlador.obtenerTodosLosLibros());
        } catch (LibroNoDisponible ex) {
            JOptionPane.showMessageDialog(null, "No hay ejemplares suficientes para prestar el libro.");
        } catch (LibroNoEncontrado ex) {
            JOptionPane.showMessageDialog(null, "ID libro incorrecto.");
        }
    }

    private void procesarAltaDeLibro() {
        Integer ejemplares = (Integer) spEjemplares.getValue();
        if (ejemplares <= 0) {
            JOptionPane.showMessageDialog(null, "Cantidad de ejemplares no puede ser negativo.");
            return;
        }
        controlador.agregarLibro(txtTitulo.getText(), txtAutor.getText(), ejemplares);
        txtTitulo.setText("");
        txtAutor.setText("");
        spEjemplares.setValue(0);
        // esta línea se va cuando esté implementado Observer
        mostrarResultadosBusqueda(controlador.obtenerTodosLosLibros());
    }

    public void mostrar() {
        frame.setVisible(true);
    }

    @Override
    public void setControlador(Controlador controlador) {
        this.controlador = controlador;
    }

    @Override
    public void mostrarAltaLibro() {

    }

    @Override
    public void mostrarDevolucionLibro() {

    }

    @Override
    public void mostrarBusquedaLibros() {

    }

    @Override
    public void mostrarPrestamoLibros() {

    }

    @Override
    public void mostrarResultadoPrestamoExitoso() {

    }

    @Override
    public void mostrarResultadoPrestamoFallido() {

    }

    @Override
    public void mostrarResultadoDevolucionExitosa() {

    }

    @Override
    public void mostrarResultadoDevolucionFallida() {

    }

    @Override
    public void mostrarResultadosBusqueda(List<Libro> librosEncontrados) {
        listModel.clear();
        for (Libro libro: librosEncontrados) {
            listModel.addElement(libro.toString());
        }

        cbLibroDevolucion.removeAllItems();
        for (Libro libro: librosEncontrados) {
            if (libro.hayPrestados()) {
                cbLibroDevolucion.addItem(libro);
            }
        }
    }

    @Override
    public void mostrarMenuPrincipal() {
        mostrarResultadosBusqueda(controlador.obtenerTodosLosLibros());
        mostrar();
    }
}
