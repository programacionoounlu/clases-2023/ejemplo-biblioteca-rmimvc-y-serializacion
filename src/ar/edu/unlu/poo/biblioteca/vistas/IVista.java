package ar.edu.unlu.poo.biblioteca.vistas;

import ar.edu.unlu.poo.biblioteca.controladores.Controlador;
import ar.edu.unlu.poo.biblioteca.modelos.Libro;

import java.util.List;

public interface IVista {
    void setControlador(Controlador controlador);
    void mostrarAltaLibro();
    void mostrarDevolucionLibro();
    void mostrarBusquedaLibros();
    void mostrarPrestamoLibros();

    void mostrarResultadoPrestamoExitoso();
    void mostrarResultadoPrestamoFallido();
    void mostrarResultadoDevolucionExitosa();
    void mostrarResultadoDevolucionFallida();

    void mostrarResultadosBusqueda(List<Libro> librosEncontrados);

    void mostrarMenuPrincipal();
}
